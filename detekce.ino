#include <DFRobotDFPlayerMini.h>
#include <SoftwareSerial.h>
#include <VirtualWire.h>
#include <LiquidCrystal.h>
LiquidCrystal lcd(7, 8, 5, 6, 3, 4);

//can be edit
unsigned long interval = 5000; // dead time after movement register
unsigned long intervalPrehrani = 5000; // time between gong play
//end

//usefull stuff
boolean prehrano = false;
boolean pohyb = false;
boolean vycisteno = false;
boolean stisknuto3 = false;
boolean stisknuto2 = false;
boolean modeTime = true;
int i = 0;
int u = 0;
int modeTimes = 3;
int modeAct = 0;
unsigned long lastPohyb = 0;
unsigned long lastKlid = 0;
unsigned long cronMillis = 0;
unsigned long currentMillis = 0;
unsigned long pushTime = 0;
unsigned long currentMillisPrehrano = 0;
unsigned long currentMillisPrehranoMode = 0;
SoftwareSerial mySoftwareSerial(A1, A0); // RX, TX
DFRobotDFPlayerMini myDFPlayer;

void setup()
{

  // Serial.begin(9600);
  pinMode(A2, INPUT_PULLUP);
  pinMode(A3, INPUT_PULLUP);
  mySoftwareSerial.begin(9600);
  myDFPlayer.begin(mySoftwareSerial);
  lcd.begin(16, 2);
  myDFPlayer.volume(30);
  welcome();
  vw_set_ptt_inverted(true);
  vw_setup(100);
  vw_set_rx_pin(2);
  vw_rx_start();

}

void loop()
{

  if (!digitalRead(A2) || !digitalRead(A3)) {
    configuration();
  }


  prijimacZprav();
  cron();
  body();
}

void configuration() {
  pushTime = millis();

  while (millis() - pushTime < 5000) {
    if (!digitalRead(A2)) {
      if (!stisknuto2) {
        i++;
        if (i > 10) {
          i = 0;
        }
        switch (i) {

          case 0:
            modeTime = true;
            intervalPrehrani = 2000;
            break;

          case 1:
            modeTime = true;
            intervalPrehrani = 5000;
            break;

          case 2:
            modeTime = true;
            intervalPrehrani = 10000;
            break;

          case 3:
            modeTime = true;
            intervalPrehrani = 20000;
            break;

          case 4:
            modeTime = true;
            intervalPrehrani = 40000;
            break;

          case 5:
            modeTime = true;
            intervalPrehrani = 120000;
            break;

          case 6:
            modeTime = false;
            modeTimes = 1;
            break;

          case 7:
            modeTime = false;
            modeTimes = 2;
            break;

          case 8:
            modeTime = false;
            modeTimes = 3;
            break;

          case 9:
            modeTime = false;
            modeTimes = 5;
            break;

          case 10:
            modeTime = false;
            modeTimes = 7;
            break;


        }
        pushTime = millis();
        stisknuto2 = true;
        cistic();

        if (modeTime) {
          configLCD(intervalPrehrani / 1000, "Opakovani zvuku", " s");

        } else {
          configLCD(modeTimes, "Opakovani zvuku", " x");
        }
      }
    }
    if (digitalRead(A2)) {
      stisknuto2 = false;
    }

    if (!digitalRead(A3)) {
      if (!stisknuto3) {
        u++;
        if (u > 5) {
          u = 0;
        }

        switch (u) {

          case 0:
            interval = 3000;
            break;

          case 1:
            interval = 5000;
            break;

          case 2:
            interval = 10000;
            break;

          case 3:
            interval = 20000;
            break;

          case 4:
            interval = 40000;
            break;

          case 5:
            interval = 120000;
            break;
        }
        pushTime = millis();
        stisknuto3 = true;
        cistic();
        configLCD(interval / 1000, "Prodleva pohybu", " s");
      }
    }
    if (digitalRead(A3)) {
      stisknuto3 = false;

    }
    delay(20);
  }
}


void configLCD(unsigned long i, String s, String t) {
  lcd.setCursor(0, 0);
  lcd.print(s);
  lcd.setCursor(0, 1);
  lcd.print("cas: ");
  lcd.print(i);
  lcd.print(t);
  vycisteno = false;
}


void body() {
  if (pohyb) {
    aktualniPohyb();
    prehraj();
    lastPohyb = millis();
  } else {
    lastKlid = millis();
    pohybPred();
    prehrano = false;
    modeAct = 0;
  }

}


void cron() {
  if (millis() - cronMillis >= 1000) {
    cistic();
    cronMillis = millis();
  }
}

void prijimacZprav() {

  uint8_t zprava[VW_MAX_MESSAGE_LEN];
  uint8_t delkaZpravy = VW_MAX_MESSAGE_LEN;

  if (vw_get_message(zprava, &delkaZpravy)) {
    pohyb = true;

    currentMillis = millis();

  } else {
    if (millis() - currentMillis >= interval) {
      pohyb = false;
    }
  }


}


void cistic() {
  if (!vycisteno) {
    lcd.clear();
    vycisteno = true;
  }
}

void prehraj() {

  if (modeTime) {

    if (!prehrano) {
      myDFPlayer.play(1);
      prehrano = true;
      currentMillisPrehrano = millis();
    }
    if (millis() - currentMillisPrehrano >= intervalPrehrani) {
      prehrano = false;
    }
  } else {

    if (millis() - currentMillisPrehranoMode >= 2500) {
      if (modeAct < modeTimes) {
        myDFPlayer.play(1);
        modeAct++;
      }
      currentMillisPrehranoMode = millis();
    }

  }
  //Serial.println(modeTime);
}

void aktualniPohyb() {
  lcd.setCursor(0, 0);
  lcd.print("Zaznamenan pohyb");
  lcd.setCursor(0, 1);
  lcd.print("pohyb trva: ");
  lcd.print(format((millis() - lastKlid) / 1000));
  vycisteno = false;
}


void pohybPred() {
  lcd.setCursor(3, 0);
  lcd.print("Neni pohyb");
  lcd.setCursor(0, 1);
  lcd.print("Pohyb pred: ");
  lcd.print(format((millis() - lastPohyb) / 1000));
  vycisteno = false;
}

//welcome screen
void welcome() {
  lcd.setCursor(1, 0);
  lcd.print("Detekce pohybu");
  lcd.setCursor(1, 1);
  lcd.print("Miroslav Skoda");
  delay(2000);
  lcd.clear();
}

String format(int times) {
  String formatted;
  if (times < 60) {
    formatted = String(times);
    formatted += " s";
  } else if (times >= 60 && times < 3600) {
    times /= 60;
    formatted = String(times);
    formatted += " m";

  } else if (times >= 3600) {
    times /= 3600;
    formatted = String(times);
    formatted += " h";
  }
  return formatted;
}

//Ceated by Miroslav Skoda @2018
//verze 0.9 7.7.2018